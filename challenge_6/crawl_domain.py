from BeautifulSoup import BeautifulSoup
import urllib2
import re
import pdb
from collections import Counter

class CrawlDomain:
    """
    def __init__(self):
        self._DOMAIN = "https://www.skiutah.com"
        self.sites_visited = {}
        self.sites_to_explore = {}

    def explore_domain_recursively(self):
        self.explore_domain(self._DOMAIN)

    def explore_domain(self, url):
        self.sites_visited[url] = True
        self.sites_to_explore.pop(url, 0)
        try:
            soup = BeautifulSoup(urllib2.urlopen(url))
            for soup_element in soup.findAll("a", attrs={'href': re.compile(self._DOMAIN)}):
                        if soup_element['href'] not in self.sites_visited.keys():        
                            self.sites_to_explore[soup_element['href']] = soup_element['href']

                    for link in self.sites_to_explore.keys():
                        print link
                        self.explore_domain(link) 

        except: #HTTP error, etc
            True
    """
    """crawl_site_map
    Purpose: Gets list of all links in sites and visits them
    Returns: Nothing
    """
    def crawl_site_map(self):
        
        soup = BeautifulSoup(urllib2.urlopen("https://www.skiutah.com/sitemap.xml")) 
        for tag in soup:
            urllib2.urlopen(tag.contents[0])
    """crawl_get_word_counts
    Purpose: Visits all internal links of site and keeps track of word counts
    Returns: Counter with all word counts
    """
    def crawl_get_word_counts(self):
        soup = BeautifulSoup(urllib2.urlopen("https://www.skiutah.com/sitemap.xml")) 
        master_counter = Counter({})
        for soup_element in soup.findAll("loc"):
            self.count_words_at_page(soup_element.contents[0], master_counter)
        return master_counter
        
    """count_words_at_page
    Purpose: Finds visible text on page and counts usage of individual words. Updates the master_counter var.
    Returns: Nothing, but changes master_counter var in the function that calls it
    """ 
    def count_words_at_page(self, url, mc):
        print url
        #Determines if text is a comment, style or something else not visible to the user
        def visible(element):
            if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
                return False
            elif re.match('<!-.*-->', str(element)):
                return False
            return True

        page_text = BeautifulSoup(urllib2.urlopen(url)) \
                        .findAll(text=True)
        visible_text = filter(visible, page_text)
        for tag in visible_text:
            word_list = re.findall(r'\b\w+', tag)
            word_list = [x.lower() for x in word_list] #Remove casing
            mc  += Counter(word_list)
        print "     Count complete"
        

crawler = CrawlDomain()
word_counts = crawler.crawl_get_word_counts()
print word_counts

