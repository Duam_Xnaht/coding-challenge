import unittest
import navigate_submenu as n
from selenium import webdriver

class TestNavigateSubmenu(unittest.TestCase):

    def test_comprehensive_positive_link_navigation(self):
        all_expected_links = [
            "Ski Utah Trip Planner",
            "Activities",
            "Food + Drink",
            "Lodging",
            "Outdoor Shops & Gear",
            "Reservation Experts",
            "Ski Resorts",
            "Ski School",
            "Transportation",
            "Utah Events",
            "Photos",
            "Videos",
            "Stories",
            "Compare Resorts",
            "Resort Comparison",
            "Alta",
            "Beaver Mountain",
            "Brian Head",
            "Brighton",
            "Cherry Peak",
            "Deer Valley",
            "Eagle Point",
            "Nordic Valley",
            "Park City",
            "Powder Mountain",
            "Snowbasin",
            "Snowbird",
            "Solitude",
            "Sundance",
            "Cross Country - Nordic Locations",
            "Snow Report",
            "Mobile App & TV Display",
            "Live Mountain Cams",
            "Printable Snow Report",
            "Why Utah Snow?",
            "All Trail Maps",
            "Powder Counter",
            "Stories",
            "All Deals",
            "Lodging",
            "Retail & Rental",
            "Transportation",
            "Food & Drink",
            "Reservations",
            "Season Specials",
            "Learn to Ski and Snowboard Month",
            "Beginner",
            "Utah Ski & Snowboard Lift Tickets",
            "5th & 6th Grade Passport",
            "Ski Utah Yeti Pass",
            "Buy The Silver and Gold Passes",
            "2016-17 Season Passes",
            "Military and Senior Day Ski Pass Prices",
            "Stories - Photos - Videos",
            "Interconnect Adventure Tour",
            "Spring Skiing",
            "Utah Areas 101",
            "Getting To Utah Resorts",
            "Backcountry Skiing",
            "Terrain Parks",
            "Gear Store",
            "Ski Utah Magazine",
            "eNewsletter & Snowmail",
            "Compare All Resorts",
            "Ski Swaps",
            "Real Estate",
            "Ski Bus",
            "Join Ski Utah",
            "Ski Utah Membership",
        ]
        for text in all_expected_links:
            try:
                driver = webdriver.Chrome()
                n.navigate_submenu(text, driver)
            except:
                driver.close()
                self.fail("ERROR: `" + text  + "` was not navigable")
            driver.close()

    def test_negative_navigate_submenu(self):
        try:
            n.navigate_submenu("This is not an existing link name")
            self.fail("Failure did not occur with non-existant link")
        except:
            self.assertTrue(True)
suite = unittest.TestLoader().loadTestsFromTestCase(TestNavigateSubmenu)
unittest.TextTestRunner(verbosity=2).run(suite)

