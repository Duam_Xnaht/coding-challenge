from selenium import webdriver

"""Go to a site
Navigates to a site and prints the name of the site 
    Parameters: A url, (optional) an expected title to match document title exactly
    Returns: Nothing
    Exceptions: Throws exception when expected title does not match page title
"""
def go_to_site(uri, title_to_verify=False):
    try:
        driver = webdriver.Chrome() 
        driver.get(uri)
        if title_to_verify:
            try:
                assert(title_to_verify == driver.title)
                print(title_to_verify + " | " + driver.title)
            except AssertionError:
                raise AssertionError
        driver.close()
    except:
        raise Exception

go_to_site("https://startpage.com", "StartPage Search Engine")

