import go_to_a_site
import unittest

class TestGoToSite(unittest.TestCase):
    #Positive test, no verificaation
    def test_can_navigate(self):
        try:
            go_to_a_site.go_to_site("https://startpage.com")
        except:
            assert(1 == 2)
    #Negative test, no verification
    def test_cannot_navigate(self):
        navigation = True
        try:
            go_to_a_site.go_to_site("xkcd")
        except:
            navigation = False
        self.assertFalse(navigation)
    #Positive test, verification
    def test_can_navigate_and_verify(self):
        navigation = True
        try:
            go_to_a_site.go_to_site("https://startpage.com", "StartPage Search Engine")    
        except:
            navigation = False
        self.assertTrue(navigation)    

    #Positive test, verification
    def test_can_navigate_and_not_verify(self):
        navigation = True
        try:
            go_to_a_site.go_to_site("https://startpage.com", "artPage Search Engine")    
        except:
            navigation = False
        self.assertFalse(navigation)    

suite = unittest.TestLoader().loadTestsFromTestCase(TestGoToSite)
unittest.TextTestRunner(verbosity=2).run(suite)
