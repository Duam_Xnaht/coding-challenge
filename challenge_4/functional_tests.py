import unittest
from selenium import webdriver
import find_airport_distance as fad

class TestNavigateSubmenu(unittest.TestCase):
    def test_positive_search(self):
        returnStr = fad.find_airport_distance_from_resorts("alta")
        self.assertTrue("Milage from" in returnStr)

    def test_negative_search(self):
        returnStr = fad.find_airport_distance_from_resorts("nonexistant resort")
        self.assertTrue("Nothing was found that matched" in returnStr)

    def test_invalid_input(self):
        returnStr = fad.find_airport_distance_from_resorts(1)
        self.assertTrue("Invalid input" in returnStr)

suite = unittest.TestLoader().loadTestsFromTestCase(TestNavigateSubmenu)
unittest.TextTestRunner(verbosity=2).run(suite)
 
