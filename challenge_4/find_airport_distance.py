import navigate_submenu as n
from selenium import webdriver
import selenium.webdriver.support.ui as ui
import selenium.webdriver.support.expected_conditions as EC
"""find_airport_distan
Parameters: resort name as a string
returns: -1 if not found or the miles from the closest major airport
"""
def find_airport_distance_from_resorts(resort, driver=webdriver.Chrome()):
    if type(resort) != type(""):
        return "Invalid input. Resort must be a number."
    #Go to comparison page
    n.navigate_submenu("Resort Comparison", driver)
    #Populate the miles from airport by selecting proper option
    select = driver.find_element_by_name("resort-feature-select")
    select.find_element_by_xpath("option[normalize-space(text())='Miles to Closest Major Airport']").click()
    #Wait for items to populate
    driver.implicitly_wait(2)
    #Get list of resorts and their milage
    resorts = driver.find_elements_by_class_name("ResortComparison-resortName")
    distance = driver.find_elements_by_class_name("ResortComparison-value")
    #Search list for resort
    for i in range(len(resorts)):
        resortName = "".join(resorts[i].get_attribute("textContent").split()) #Remove whitespace
        #print resortName.lower() + " compared to " + resort.lower()
        if resortName.lower() == resort.lower():
            return "Milage from " + resort + " is " + "".join(distance[i].get_attribute("textContent").split())
    return "Nothing was found that matched " + resort #If we made it here, the resort does not exist
