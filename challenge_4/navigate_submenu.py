from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

"""navigate_submenu
Exposes links in header menu, searches for a link matching the specified text and navigates to it
Parameters: text content of link to navigate to
            optional driver can be injected, defaults to chrome
Returns: nothing
"""
def navigate_submenu(submenu_text, driver=None):
    if driver is None:
        driver = webdriver.Chrome()
    driver.get("https://skiutah.com")
    banner = driver.find_element_by_id("top_menu")
    driver.execute_script(
    """
    //Find element with header links
    topMenu = document.getElementById("top_menu");
    headerLinks = topMenu.children[0].children;

    //Go through each header, exposing the submenu

    for (i=0; i < headerLinks.length; i++) {
        submenu = headerLinks[i].children[1];
        //Show the submenu
        try {
            submenu.style = "display:block";
        } catch (no_sub_menu){ //Don't crash if there isn't a submenu
            continue;
        }
    }
    """
    )
    #All submenus have been exposed. Now all we have to do is see if the link exists and click
    submenu = banner.find_element_by_link_text(submenu_text)
    #Some elements aren't clickable. Navigate to href instead
    href = submenu.get_attribute("href")
    driver.get(href)
