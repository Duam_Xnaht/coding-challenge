#coding=utf-8
import unittest
import navigate_menu as n

class TestNavigateMenu(unittest.TestCase):
    def test_positive_exact_link_casing(self):
        self.assertTrue(n.navigate_menu("DEALS", u"Ski and Snowboard The Greatest Snow on Earth® - Ski Utah"))
    def test_positive_mixed_link_casing(self):
        self.assertTrue(n.navigate_menu("Deals", u"Ski and Snowboard The Greatest Snow on Earth® - Ski Utah"))
    def test_negative_bad_validation(self):
        self.assertFalse(n.navigate_menu("Deals", u"Skreatest Snow on Earth® - Ski Utah"))
    def test_negative_bad_link(self):
        try:
            n.navigate_menu("garbage_link", "No way to validate this string")
        except:
            return True
        self.assertTrue(False)
suite = unittest.TestLoader().loadTestsFromTestCase(TestNavigateMenu)
unittest.TextTestRunner(verbosity=2).run(suite)

