#coding=utf-8
from selenium import webdriver


def navigate_menu(navigateTo, validationString, driver = None):
    if driver is None:
        driver = webdriver.Chrome()
    driver.get("https://www.skiutah.com") 
    href = driver.find_element_by_link_text(navigateTo.upper()).get_attribute("href")
    driver.get(href)
    result = True if driver.title == validationString else False
    driver.close()
    return result
