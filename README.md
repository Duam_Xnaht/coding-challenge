# Various coding challenges for STG Consulting evaluation
The coding solutions are implemented as python (2.7) scripts. Unit or functional tests have been written for all of them using unittest. To run the tests you may need to install the python module unittests. The other dependency is selenium. Tests are easily run by executing the "unittests.py" file
