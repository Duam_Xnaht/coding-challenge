import unittest
import search_with_results as swr
from search_with_results import *
from selenium import webdriver


class TestNavigateMenu(unittest.TestCase):

    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.LISTING_SITE = "https://www.skiutah.com/members/listing"
        self.srch = swr.SearchWithResults()

    def tearDown(self):
        self.driver.close()

    #GET_TOTAL_NUMBER_RESULTS
    def test_run_get_results_on_expected_listing_page(self):
        test_pass = False
        self.srch.driver.get("https://www.xkcd.com")
        try:
            self.srch.get_total_number_of_results()
        except swr.OnWrongWebPage:
            test_pass = True
        if not test_pass:
            self.fail("Driver did not throw error when search performed on different website")

           
    def test_run_when_not_on_expected_listing_page(self):
        test_pass = True
        self.driver.get("https://www.xkcd.com")
        try:
            self.srch.perform_search()
        except Exception:
            test_pass = False
        self.assertTrue(test_pass)
         
    #Verify that page changes items
    #changing search criteria changes results
    def test_result_numbers_change_with_search(self):
        self.driver.get(self.LISTING_SITE)
        #A search with parameters of All, All, All (default values) 
        #Should return more values than a selective search 
        self.srch.perform_search() #Search for all values
        total_listings = self.srch.get_total_number_of_results()
        self.srch.perform_search("Reservation Experts") #Search for more exclusive set
        expected_less_listings = self.srch.get_total_number_of_results()
        self.assertTrue(expected_less_listings < total_listings)


    #Invalid input
    def test_parameters_only_strings(self):
        test_pass = True
        try:
            self.srch.perform_search("", 1, "")
            test_pass = False
        except ValueError:
            True
        try:
            self.srch.perform_search(1, "foo", "bar")
            test_pass = False
        except ValueError:
            True
        try:
            self.srch.perform_search("foo", "bar", 1)
            test_pass = False
        except ValueError:
            True
        try:
            self.srch.perform_search("foo", type(1), "bar")
            test_pass = False
        except ValueError:
            True
        try:
            self.srch.perform_search(type(1), "foo", "bar")
            self.srch.perform_search("foo", "bar", type(1))
            test_pass = False
        except ValueError:
            True
        self.assertTrue(test_pass)

    
        #Make sure that get_results can tell if there is more than one page number
        def test_get_results_sees_multiple_page_numbers(self):
            self.srch.perform_search()
            self.assertTrue(self.srch.get_total_pages_of_results() > 1)

        #PERFORM SEARCH
        #Subcategory and what specified
        #All results- returns large number of items
        #No search results-Can't find combination to generate this
        
suite = unittest.TestLoader().loadTestsFromTestCase(TestNavigateMenu)
unittest.TextTestRunner(verbosity=2).run(suite)

