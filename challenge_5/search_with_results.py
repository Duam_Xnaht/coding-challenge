from selenium import webdriver
from selenium.webdriver.support.ui import Select

class OnWrongWebPage(Exception):
    pass

class SearchWithResults:

    def __init__(self, driver=webdriver.Chrome()):
        self.driver = driver
        self.LISTING_PAGE = "https://www.skiutah.com/members/listing"

    """perform_search
    fills out form parameters and does search
    Parameters: Search categories as stringss- what, sub category, resort
    Note: Specifying a subcategory will supercede the 'what' option 
    Returns: Nothing
    """
    def perform_search(self, what="All", subcategory="All", resort="All"):
        #Validate parameters are strings
        if type("a string") != type(what) != type(subcategory) != type(resort):
            raise ValueError("An input parameter was not a string: \n%s\n%s\n%s", what, subcategory, resort)
        #if self.driver.current_page != self.LISTING_PAGE:
        #    self.driver.get(self.LISTING_PAGE)    
        self.driver.get(self.LISTING_PAGE)    
        
        #Get select elements
        filter_category_input = Select(self.driver.find_element_by_name("filter-category-select"))
        subcategory_input = Select(self.driver.find_element_by_name("filter-sub-category-select"))
        resort_input = Select(self.driver.find_element_by_name("filter-resort-select"))

        #Attempt to change select elements to match inputs
        try:
            if what != "All":
                filter_category_input.select_by_visible_text(what)
            if subcategory != "All":
                subcategory_input.select_by_visible_text(subcategory)
            if resort != "All":
                resort_input.select_by_visible_text(resort)
        except Exception:
            raise ValueError("Select elements used for searching could not be found")
        #Perform search
        self.driver.find_element_by_name("filter-form-submit").click()

    """get_results
    Gets the text of the search results
    Parameters: None
    Returns: List of web elements
    """
    def get_results(self):
        if self.LISTING_PAGE not in self.driver.current_url:
            raise OnWrongWebPage("Not on the expected listing page: " + self.driver.current_url + "\n" + self.LISTING_PAGE)
    
        #Find total number of pages of results
        total_pages = self.get_total_pages_of_results()
        #If needed, get url to modify to return new list items
        search_link = None
        beginning_URL = None
        end_URL = None
        if total_pages > 1:
            #create urls we can insert a page number between to navigate list results
            
            #u'https://www.skiutah.com/members/listing/type/Lodging/resort/brighton?filter-category-select=Lodging&filter-resort-select=brighton&pagenumber:int=2&ti:int=0&tif:int=0#load-more-items'
           
            search_link = self.driver.find_elements_by_class_name("BatchLinks-pageNumber")[1].get_attribute("href")
            search_link = search_link.split('pagenumber:int=')
            beginning_URL = search_link[0] + 'pagenumber:int='
            end_URL = search_link[1].split('&')
            end_URL = end_URL[1:] #drop first element in the list
            end_URL = '&' + '&'.join(end_URL)
    
        results = []
        #Navigate each page, scraping the test of each listing item
        if total_pages != 1:
            for i in range(total_pages):
                #modify url to navigate to each page (less dependencies on web elements)
                self.driver.get(beginning_URL + str(i + 1) +  end_URL)
                #Grab the list items
                listing_items = self.driver.find_elements_by_class_name("ListingResults-item")
                for i in range(len(listing_items)):
                    results.append(listing_items[i].get_attribute("textContent"))
            #Return list of listing items and their text
        else:
            #Grab the list items
            listing_items = self.driver.find_elements_by_class_name("ListingResults-item")
            for i in range(len(listing_items)):
                results.append(listing_items[i].get_attribute("textContent"))
        return results
            

    """get_total_pages_of_results
    gets the total number of pages created by pagination
    Parameters: None
    Returns: integer (starting from 1)
    """
    def get_total_pages_of_results(self):
        if  self.LISTING_PAGE not in self.driver.current_url:
            raise OnWrongWebPage("Not on the expected listing page: " + self.driver.current_url + "\nexpected " + self.LISTING_PAGE)
        pageSpan = None 
        try:
            pageSpan = self.driver.find_element_by_css_selector(".BatchLinks-pageNumber.BatchLinks-pageNumber--current")
        except:
            return 1 #Only one page of results in this case
        #Get pagination numbers in a single line of text
        paginationString = self.driver.find_element_by_css_selector(".Grid-cell.u-sm-size6of10.u-size8of10.u-textCenter") 
        #Remove the many newlines from string and remove leading and ending whitespace
        paginationString = paginationString.get_attribute("textContent").replace("\n", "").strip()

        #The number of pages should now be the last number in an array if we use split
        pagination = paginationString.split(" ")
        totalNumbers = int(pagination[len(pagination) - 1])

        assert totalNumbers > 1
        return totalNumbers

    """get_total_number_of_results
    Gets the total number of returned results from page
    Parameters: None
    Returns: Integer
    """
    def get_total_number_of_results(self):
            if self.LISTING_PAGE not in self.driver.current_url:
                raise OnWrongWebPage("Not on the expected listing page: " + self.driver.current_url + "\nexpected " + self.LISTING_PAGE)

            total_listings = self.driver.find_element_by_class_name("ListingResults-count") \
                                .get_attribute("textContent") \
                                .strip() #Should be in format 'n of m results'
            total_listings = total_listings.split("\n")[2] #get the total number
            total_listings = int(total_listings.split(" ")[-2])
            if type(total_listings) != type(0):
                raise TypeConversionError("Was not able to convert listing results to number")
            return total_listings

#SearchWithResults().perform_search().get_total_number_of_results()
#SearchWithResults().get_total_number_of_results()

#print len(SearchWithResults().get_results())


