import unittest
import search_with_results as swr

class TestNavigateMenu(unittest.TestCase):
    #Get results-
    def test_get_results_best_representative(self):
        #Best representative 
        #One page of results
        srch = swr.SearchWithResults() 
        srch.perform_search("Activities", "Entertainment")
        results = srch.get_results()
        #See if we got results back in strings (nonempty)
        for i in range(len(results)):
            if type(results[i]) != type(u""):
                self.fail("Non-string found in results")
            if len(results[i]) == 0:
                self.fail("An item was returned as an empty string in get_results()") 

    #perform search
        #Subcategory and what specified
        #All results
        #Two pages of results
    def test_boundary_values_2_pages(self):
        srch = swr.SearchWithResults()
        srch.perform_search("Ski School", "Clinics")
        results = srch.get_results()
        self.assertTrue(len(results) > 18 and len(results) <= 36) #Ensure that there are two pages worth of input, and that they were grabbed

    #Long sequence
    #Also grabs 3+ pages of results
    def test_long_sequence(self):
        srch = swr.SearchWithResults()
        #See if running search multiple times changes results returned
        srch.perform_search()
        total_results = srch.get_total_number_of_results()
        srch.perform_search("Resort")
        total_resort = srch.get_total_number_of_results()
        srch.perform_search("All", "Bar", "All")
        total_bar = srch.get_total_number_of_results()
        
        #Get verification data
        srch.perform_search()
        total_results_verification = srch.get_total_number_of_results()
        srch.perform_search("Resort")
        total_resort_verification = srch.get_total_number_of_results()
        srch.perform_search("All", "Bar", "All")
        total_bar_verification = srch.get_total_number_of_results()
    
        self.assertTrue(total_results == total_results_verification)
        self.assertTrue(total_bar == total_bar_verification)
        self.assertTrue(total_resort == total_resort_verification)
       
suite = unittest.TestLoader().loadTestsFromTestCase(TestNavigateMenu)
unittest.TextTestRunner(verbosity=2).run(suite)

